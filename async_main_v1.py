import json

import aiofiles as aiofiles
import aiohttp as aiohttp
from fake_useragent import UserAgent
import requests
from bs4 import BeautifulSoup

import asyncio
import tqdm  # <-- I use this for nice progress bar/timing
from time import perf_counter



mk_url = "https://mnogoknig.lv/ru/products/"
mk_books = []
mk_max_id = 1314702

ua = UserAgent()
headers = {'user-agent': f'{ua.random}'}


async def fetch(s, url):
    try:
        async with s.get(mk_url + str(url)) as r:
            # print(r)
            # if r.status != 200:
            #     r.raise_for_status()
            # content = r.text()
            # print(content)
            return BeautifulSoup(await r.text(), "lxml")
    except Exception as e:
        print(f"Error: {e}. Skipping this request and continuing...")
        return None


async def fetch_all(s, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(s, url))
        # print(task)
        tasks.append(task)
    res = await asyncio.gather(*tasks)
    for book in tqdm.tqdm(res):
        if book is not None:
            title = book.find("h1").text
            print(title)
            if (title == "Whoops, looks like something went wrong."):
                pass
            else:
                price = book.find("div", class_="productprice")
                if price:
                    price = price.text.strip("€")
                else:
                    price = 'None'
                author = book.find("span", itemprop="author")
                if author:
                    author = author.text
                else:
                    author = 'None'
                try:
                    id = book.find("meta", attrs={'itemprop':'mpn'})["content"]
                except (TypeError, KeyError):
                    id = None
                try:
                    url = book.find("link",  attrs={'itemprop':'url'})["href"]
                except (TypeError, KeyError):
                    url = None
                # print("id:", id, "Title: ", title, ", price: ", price, ", author: ", author, ", url: ", url)
                new_book = {"id": id, "title": title, "price": price, "author": author, "url": url}
                mk_books.append(new_book)
                # print('added')
                # print(mk_books)
        else:
            print("The book object is None.")

    print("Writing to file")
    # print(mk_books)
    with open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\mk_books.json", "w", encoding="UTF-8") as f:
        json.dump(mk_books, f, ensure_ascii=False)

async def main():
    start = perf_counter()
    urls = range(1000000, 1050000)

    async with aiohttp.ClientSession() as session:
        htmls = await fetch_all(session, urls)
        # print(htmls)

    stop = perf_counter()
    print("time taken:", stop - start)

if __name__ == '__main__':
    # start = perf_counter()
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
    # stop = perf_counter()
    # print("time taken:", stop - start)