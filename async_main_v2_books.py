import json

import aiofiles as aiofiles
import aiohttp as aiohttp
from fake_useragent import UserAgent
import requests
from bs4 import BeautifulSoup

import asyncio
import tqdm  # <-- I use this for nice progress bar/timing
from time import perf_counter

books = []
new_categories = []
books_list = []

ua = UserAgent()
headers = {'user-agent': f'{ua.random}'}


async def fetch(s, url):
    try:
        async with s.get(url) as r:
            print(r)
            data = BeautifulSoup(await r.text(), "lxml")
            try:
                row = data.find('div', attrs={'class': 'row products'})
                # products = row.findAll('div', attrs={'class': 'product'})
                products = row.findAll('a')
                for p in products:
                    # print(p)
                    link = p.get('href')

                    if link not in books:
                        books.append(link)
                        # print(len(books))
            except Exception as E:
                print(E)

            async with aiofiles.open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\mk_books_list_2.json", "a",
                                     encoding="UTF-8") as f:
                await f.write(json.dumps(link, ensure_ascii=False))

            return None

    except Exception as e:
        print(f"Error: {e}. Skipping this request and continuing...")
        return None


async def fetch_all(s, urls):
    tasks = []
    for url in urls:
        if (url['max_page'] != 0):
            for i in range(2, int(url['max_page']) + 1):
                task = asyncio.create_task(fetch(s, url['url'] + '?page=' + str(i)))
                tasks.append(task)
        task = asyncio.create_task(fetch(s, url['url']))
        # print(task)
        tasks.append(task)
    res = await asyncio.gather(*tasks)

    # for pages in tqdm.tqdm(res):
    #     try:
    #         for page in pages:
    #             row = page.find('div', attrs={'class': 'row products'})
    #             # products = row.findAll('div', attrs={'class': 'product'})
    #             products = row.findAll('a')
    #             for p in products:
    #                 # print(p)
    #                 link = p.get('href')
    #
    #                 if link not in books:
    #                     books.append(link)
    #                     # print(len(books))
    #
    #     except Exception as E:
    #         print(E)

    print("Writing to file")
    # print(books)
    # with open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\mk_books_list.json", "w+", encoding="UTF-8") as f:
    #     json.dump(books, f, ensure_ascii=False)

async def main():
    start = perf_counter()

    f = open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\mk_categories_3.json")
    categories = json.load(f)
    # categories = []
    # for c in categories:
    #     print(c)


    async with aiohttp.ClientSession() as session:
        htmls = await fetch_all(session, categories)

    stop = perf_counter()
    print("time taken:", stop - start)

if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())