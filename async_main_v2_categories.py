import json

import aiofiles as aiofiles
import aiohttp as aiohttp
from fake_useragent import UserAgent
import requests
from bs4 import BeautifulSoup

import asyncio
import tqdm  # <-- I use this for nice progress bar/timing
from time import perf_counter



category_url = "https://mnogoknig.lv/ru/category/"
categories = []

ua = UserAgent()
headers = {'user-agent': f'{ua.random}'}

async def fetch(s, url):
    try:
        async with s.get(category_url + str(url)) as r:
            return BeautifulSoup(await r.text(), "lxml")
    except Exception as e:
        print(f"Error: {e}. Skipping this request and continuing...")
        return None


async def fetch_all(s, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(s, url))
        # print(task)
        tasks.append(task)
    res = await asyncio.gather(*tasks)

    for category in tqdm.tqdm(res):
        try:
            url = category.find("meta", attrs={'property':'og:url'})["content"]
            print(url)
            categories.append(url)
        except Exception as E:
            print(E)

    print("Writing to file")
    # print(mk_books)
    with open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\categories.json", "w", encoding="UTF-8") as f:
        json.dump(categories, f, ensure_ascii=False)

async def main():
    start = perf_counter()
    urls = range(0, 1000)

    async with aiohttp.ClientSession() as session:
        htmls = await fetch_all(session, urls)
        # print(htmls)

    stop = perf_counter()
    print("time taken:", stop - start)

if __name__ == '__main__':
    # start = perf_counter()
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
    # stop = perf_counter()
    # print("time taken:", stop - start)