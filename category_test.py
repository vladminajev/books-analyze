import json

import aiofiles as aiofiles
import aiohttp as aiohttp
from fake_useragent import UserAgent
import requests
from bs4 import BeautifulSoup

import asyncio
import tqdm  # <-- I use this for nice progress bar/timing
from time import perf_counter



category_url = "https://mnogoknig.lv/ru/category/"
categories = []

ua = UserAgent()
headers = {'user-agent': f'{ua.random}'}

def mk_collect_data():
    start = perf_counter()

    for i in range(0, 1000):
        # print(i)
        url = category_url + str(i)
        # print(url)
        response = requests.get(url, headers=headers)
        # print(response)
        if(response.status_code == 500):
            continue
        content = response.text
        # print(content)

        soup = BeautifulSoup(content, "lxml")
        try:
            url = soup.find("meta", attrs={'property':'og:url'})["content"]
            print(url)
            categories.append(url)
        except Exception as E:
            print(E)


    stop = perf_counter()
    print("time taken:", stop - start)
    print(categories)


def sub_categories():
    start = perf_counter()

    f = open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\categories.json")

    # returns JSON object as
    # a dictionary
    data = json.load(f)
    # print(data)

    for c in data:
        response = requests.get(c, headers=headers)
        # print(response)
        if (response.status_code == 500):
            continue
        content = response.text
        # print(content)

        soup = BeautifulSoup(content, "lxml")
        try:
            data = soup.findAll('div', attrs={'class': 'categories-horizontal'})
            for div in data:
                links = div.findAll('a')
                for a in links:
                    print(a['href'])

                categories.append(a['href'])
        except Exception as E:
            print(E)


    with open(r"C:\Users\vladi\Desktop\Python\mnogoKnig\sub_categories.json", "w", encoding="UTF-8") as f:
        json.dump(categories, f, ensure_ascii=False)

    stop = perf_counter()
    print("time taken:", stop - start)

def main():
   # mk_collect_data()
    sub_categories()

if __name__ == '__main__':
    main()