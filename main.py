from fake_useragent import UserAgent
import requests
from bs4 import BeautifulSoup

from time import perf_counter


mk_url = "https://mnogoknig.lv/ru/products/"
mk_books = []

ua = UserAgent()

headers = {'user-agent': f'{ua.random}'}
# print(headers)

def mk_collect_data():
    start = perf_counter()
    max_id = 1314702

    for i in range(max_id, 0, -1):
        # print(i)
        url = mk_url + str(i)
        # print(url)
        response = requests.get(url, headers=headers)
        # print(response)
        if(response.status_code == 500):
            continue
        content = response.text
        # print(content)

        soup = BeautifulSoup(content, "lxml")

        title = soup.find("h1").text
        if(title == "Whoops, looks like something went wrong."):
            pass
        else:
            price = soup.find("div", class_="productprice")
            if price:
                price = price.text.strip("€")
            else:
                price = 'None'
            author = soup.find("span", itemprop="author")
            if author:
                author = author.text
            else:
                author = 'None'
            # print("Url: ", url, "Title: ", title, ", price: ", price, ", author: ", author, sep="")
            new_book = {"id": i, "url": url, "title": title, "price": price, "author": author}
            mk_books.append(new_book)
            # print(mk_books)
    stop = perf_counter()
    print("time taken:", stop - start)

def main():
   mk_collect_data()

if __name__ == '__main__':
    main()
